#!/bin/bash

board="esp8266:esp8266:generic"
config=".cli-config.yml"
#bin/arduino-cli core --config-file $config update-index
#bin/arduino-cli core --config-file $config core install $board
bin/arduino-cli core install esp8266:esp8266 --additional-urls http://arduino.esp8266.com/stable/package_esp8266com_index.json
echo 'Look to the makefile for more information'
