#!/bin/bash
target=$1
[ -z $target ] && exit 0

npc='\11\12\15\40-\176'
scrub=$(cat $target | tr -cd $npc | sed -r '/^.{,63}$/d;/^.{66}./d;s/\| //g' | awk '{print $2 " "}')

count=$(echo "$scrub" | wc -l)
echo -e "Signals capture: $count"

unique=$(echo "$scrub" | uniq | wc -l)
echo -e "Unique signals captured: $unique"
