#!/bin/bash

# most of the time this should be the right port
port='ttyACM0'

# better watch script 
while true; do
	case "$1" in 
		h|hex)
			echo "Watching hex scroll by on $port"
			while true; do cat /dev/$port | hd ;done #hexdump -e '16/1 "%02x " "\n"' ; done 
			;;
		*)
			echo "Default option: watching text on $port"
			while true; do cat /dev/$port; done
	esac
done
