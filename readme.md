# ESP8266 Promiscuous Library

The _bleeding edge_ code is on the current active branch: `packet` where there is likely going to be more up to date codebase.

## Pins - Wifi

To connect with the wifi antenna follow the diagram below.

![pins-wifi](wifi-pins.png)

In case something goes wrong there are a few things to note:

1. Just like the arduino the ESP8266 needs to be reset occasionally. To do this without any equipement just unplug the devices from _all_ power sources, wait about 30sec - 1 min(ish).

2. GPIO-0 must be connected to ground before you power the system back up. This causes a short which sends the chip into flash mode where you can upload sketches.

3. Keep GPIO-0 on ground when its back on for this since with it out we get random garbage data more often.

|	     	|	    	|
|	-----	|	----	|
| 	RX		|	VCC		|
|	GPIO-0	| 	Reset	|
|	GPIO-2	|	CH\_EN	|
|	Ground	|	Tx		|

## Tracklist

_no particular order_

* callback cleanup

	* ensuring that any callbacks in the arduino library we're making are more controlled

### Technical Todo Things

* Autorun on boot 

	* flash data to the arduino and have the arduino act as a controller for the ESP8266 without having to reflash on every reset.

	* Flash pin being pulled high constantly

## Linux Directories

```
~/.arduino15/packages/esp8266/hardware/esp8266/2.4.2/tools/sdk/include/
```

Should contain `user_interface.h` where we can find the exposed library functions to enable `promiscuous mode`
