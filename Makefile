# Example sketch using packet/ as the target directory
PROJ='packet/'
# Assuming you have this bit installed you don't need the IDE installed at all
CC='bin/arduino-cli'
# Default port for Ubuntu - double check your distro's default port for usb serial connections
PORT='/dev/ttyACM0'
# Since we don't care to use the arduino itself we just target the espitself
#BOARD='arduino:avr:uno'
BOARD='esp8266:esp8266:generic'

compile:
	$(CC) compile --fqbn $(BOARD) $(PROJ)

up:
	$(CC) upload -p $(PORT) --fqbn $(BOARD) $(PROJ)

# couldnt think of a better name for this pls no bully
yeet: compile up

setup:
	sh scripts/setup.sh

clean:
	rm -f packet/packet.esp8266.esp8266.generic.bin
	rm -f packet/packet.esp8266.esp8266.generic.elf

