/* esp8266 code is compiled using g++ so we need C labels/directives since _it's_ written in c for */
#ifndef _SDK_STRUCTS_H_ || _IEEE80211_STRUCTS_H
	#define _SDK_STRUCTS_H_
	#define _IEE80211_STRUCTS_H_

#include <ESP8266WiFi.h>

				/******************* DATA PACKETS *******************/
				/******************* DATA PACKETS *******************/
				/******************* DATA PACKETS *******************/
				/******************* DATA PACKETS *******************/

/******************* CONTROL STRUCTURE *******************/

typedef struct wifi_pkt_rx_ctrl_t {
	signed rssi:8;	/* strength of signal */
	unsigned rate:4;

	unsigned is_group:1;
	unsigned sig_mode :2;
	unsigned legacy_length:12;

	unsigned damatch0:1;
	unsigned damatch1:1;

	unsigned bssidmatch0:1;
	unsigned bssidmatch1:1;

	unsigned MCS:7;
	unsigned CWB:1;

	unsigned HT_length:16;
	unsigned smoothing:1;
	unsigned Not_Sounding:1;
	unsigned:1;	// padding
	unsigned Aggregation:1;
	unsigned STBC:2;
	unsigned FEC_CODING:1;	// big/little endianness

	unsigned SGI:1;
	unsigned rxend_state:8;
	unsigned ampdu_cnt:8;
	unsigned channel:4;		/* channel that the packet is sent over */
	unsigned:12;
};

/******************* DATA PACKETS *******************/

typedef struct wifi_pkt_lenseq_t {
	uint16_t length;
	uint16_t seq;
	/* 6 byte target address */
	uint8_t address3[6];
};

typedef struct wifi_pkt_data_t {
	wifi_pkt_rx_ctrl_t rx_ctrl;
	uint8_t buff[36];
	uint16_t cnt;
	wifi_pkt_lenseq_t lenseq[1];
};



/******************* MANAGEMENT PACKETS *******************/

struct wifi_pkt_mgmt_t {
	wifi_pkt_rx_ctrl_t rx_ctrl;
	uint8_t buff[112];	/* */
	uint16_t cnt;
	uint16_t len;		/*length of target packet*/
};

/******************* PROMISCUOUS PACKET *******************/
typedef struct wifi_promiscuous_pkt_t {
	wifi_pkt_rx_ctrl_t rx_ctrl;
	/* payload determined by rx_ctrl.sig_len^ */
	uint8_t payload[0];	/* Variable Length Array in the wild */
};

			/******************* IEEE STRUCTURES  *******************/
			/******************* IEEE STRUCTURES  *******************/
			/******************* IEEE STRUCTURES  *******************/
			/******************* IEEE STRUCTURES  *******************/

/*
#ifndef _IEE80211_STRUCTS_H_
	#define _IEE80211_STRUCTS_H_
*/
typedef enum wifi_promiscuous_pkt_type_t {
    WIFI_PKT_MGMT,
    WIFI_PKT_CTRL,
    WIFI_PKT_DATA,
    WIFI_PKT_MISC,
};


/******************* MANAGEMENT SUBTYPES *******************/

typedef enum wifi_mgmt_subtypes_t {
    ASSOCIATION_REQ,
    ASSOCIATION_RES,
    REASSOCIATION_REQ,
    REASSOCIATION_RES,
    PROBE_REQ,
    PROBE_RES,
    NU1,  /* ......................*/
    NU2,  /* 0110, 0111 not used */
    BEACON,
    ATIM,
    DISASSOCIATION,
    AUTHENTICATION,
    DEAUTHENTICATION,
    ACTION,
    ACTION_NACK,
};

typedef struct wifi_mgmt_beacon_t {
  unsigned interval:16;
  unsigned capability:16;
  unsigned tag_number:8;
  unsigned tag_length:8;
  char ssid[0];
  uint8 rates[1];
}; 

typedef struct wifi_header_frame_control_t {
    unsigned protocol:2;
    unsigned type:2;
    unsigned subtype:4;
    unsigned to_ds:1;
    unsigned from_ds:1;
    unsigned more_frag:1;
    unsigned retry:1;
    unsigned pwr_mgmt:1;
    unsigned more_data:1;
    unsigned wep:1;
    unsigned strict:1;	
};

typedef struct wifi_ieee80211_mac_hdr_t {
	wifi_header_frame_control_t frame_ctrl;
	uint8_t addr1[6];	// receiver addr
	uint8_t addr2[6];	// sender addr 
	uint8_t addr3[6];	// filtering addr
	unsigned sequence_ctrl:16;
	uint8_t addr4[6];	// optional addr
};

typedef struct wifi_ieee80211_pkt_t {
	wifi_ieee80211_mac_hdr_t hdr; 
	uint8_t payload[2];	// network data  ends with 4 bytes
};
#endif
