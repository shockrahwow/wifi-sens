/* wifistrings.h */
#include "struct.h"

#ifndef MAC_BUFFER_SIZE 
	#define MAC_BUFFER_SIZE 12
#endif

/* writes mac address to given buffer */
static void convert_mac(char* buffer, const size_t bsize, const uint8_t* data);

/* buffer must be 6 bytes long for each mac address */
void print_mac(char* buffer, const size_t bsize, const uint8_t* data);

/* Signal strenght of given packet */
int signal_strength(const wifi_promiscuous_pkt_t* pkt);

/* String of wifi packet type */
char* wifi_packet_type_str(const wifi_promiscuous_pkt_type_t type, const wifi_mgmt_subtypes_t subtype);
