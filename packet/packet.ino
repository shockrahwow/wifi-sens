#include <ESP8266WiFi.h>
#include "promiscuous.h"	 

extern "C" {
	#include "user_interface.h"
}

void setup() {
	// Preferred baud rate for the ESP8266 
	Serial.begin(115200);
	delay(100);

	// First we should disconnect from any networks we may be a part of 
	WiFi.disconnect();

	// Next we'll set the ESP to station mode to allow promiscuous mode
	wifi_set_opmode(STATION_MODE);

	prom_set_handler(NULL);	// just using the default packet handler for now

	wifi_promiscuous_enable(1);

	// Channel hopping can be implemented in the loop function below though its been left blank 
	// for the sake of simplicity here
	wifi_set_channel(9);

}

// channel hopping 
void loop() {
	delay(100);	// timing things
}
